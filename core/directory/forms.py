"""
forms for :mod:`directory` application.

:creationdate:  12/02/2023 20:04
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: directory.forms
"""
import logging
from django import forms
from django.utils.translation import gettext as _
from directory import models

__author__ = "fguerin"
logger = logging.getLogger(__name__)


class SearchForm(forms.Form):
    """Simple form for students search."""

    name = forms.CharField(
        max_length=255,
        label=_("Name"),
        required=False,
    )


class StudentCreateForm(forms.ModelForm):
    """For to create a new Student instance."""

    class Meta:
        """Metaclass for :class:`directory.forms.StudentCreateForm`."""

        model = models.Student
        exclude = []


class StudentUpdateForm(forms.ModelForm):
    """For to create a new Student instance."""

    class Meta:
        """Metaclass for :class:`directory.forms.StudentUpdateForm`."""

        model = models.Student
        exclude = []
