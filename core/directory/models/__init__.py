"""
Models for the directory app.

DA21 2022 django application demo project.

"""
import logging
from django.utils.translation import gettext_lazy as _
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from .helpers import Administrable

logger = logging.getLogger(__name__)


class Level(models.TextChoices):
    """Level of the classes."""

    BEGINNER = "BEGINNER", _("Beginner")
    INTERMEDIATE = "INTERMEDIATE", _("Intermediate")
    ADVANCED = "ADVANCED", _("Advanced")


class ClassLevel(Administrable):
    """A class level."""

    name = models.CharField(
        max_length=255,
        verbose_name=_("Name"),
    )

    level = models.CharField(
        max_length=20,
        choices=Level.choices,
        verbose_name=_("Level"),
    )

    class Meta:
        """Class meta for ClassGroup."""

        verbose_name = _("Class Group")
        verbose_name_plural = _("Class Groups")

    def __str__(self):
        """Return the string representation of the class level."""
        try:
            level = Level(self.level).label
        except ValueError:
            level = self.level
        return f"{self.name} ({level})"


class ClassOption(Administrable):
    """A class option."""

    name = models.CharField(
        max_length=255,
        verbose_name=_("Name"),
    )

    description = models.TextField(
        verbose_name=_("Description"),
        blank=True,
    )

    class_level = models.ForeignKey(
        ClassLevel,
        on_delete=models.CASCADE,
        verbose_name=_("Class Level"),
        related_name="options",
    )

    class Meta:
        """Class meta for ClassOption."""

        verbose_name = _("Class Option")
        verbose_name_plural = _("Class Options")

    def __str__(self):
        """Return the string representation of the class option."""
        return f"{self.name} ({self.class_level})"


class Civility(Administrable):
    """A civility."""

    name = models.CharField(
        max_length=255,
        verbose_name=_("Name"),
    )

    short_name = models.CharField(
        max_length=255,
        verbose_name=_("Short Name"),
    )

    class Meta:
        """Class meta for Civility."""

        verbose_name = _("Civility")
        verbose_name_plural = _("Civilities")

    def __str__(self):
        """Return the string representation of the civility."""
        return f"{self.name} ({self.short_name})"


class Student(Administrable):
    """A student."""

    civility = models.ForeignKey(
        Civility,
        on_delete=models.PROTECT,
        verbose_name=_("Civility"),
        related_name="students",
    )

    last_name = models.CharField(
        max_length=255,
        verbose_name=_("Last Name"),
    )

    first_name = models.CharField(
        max_length=255,
        verbose_name=_("First Name"),
    )

    birth_date = models.DateField(
        verbose_name=_("Birth date"),
    )

    photo = models.ImageField(
        upload_to="images/",
        verbose_name=_("Photo"),
        blank=True,
        null=True,
    )

    allow_public_photo = models.BooleanField(
        default=False,
        verbose_name=_("Allow public photo"),
    )

    # region Contact
    email = models.EmailField(
        verbose_name=_("Email"),
        blank=True,
        default="",
    )

    phone = PhoneNumberField(
        verbose_name=_("Phone"),
        blank=True,
        default="",
    )
    # endregion Contact

    # region Address
    address = models.CharField(
        max_length=255,
        verbose_name=_("Address"),
        blank=True,
        default="",
    )

    address_complement = models.CharField(
        max_length=255,
        verbose_name=_("Address complement"),
        blank=True,
        default="",
    )

    city = models.CharField(
        max_length=255,
        verbose_name=_("City"),
        blank=True,
        default="",
    )

    zip_code = models.CharField(
        max_length=255,
        verbose_name=_("Zip Code"),
        blank=True,
        default="",
    )
    # endregion Address

    # region Class data

    class_level = models.ForeignKey(
        ClassLevel,
        on_delete=models.PROTECT,
        verbose_name=_("Class Level"),
        related_name="students",
    )

    class_options = models.ManyToManyField(
        ClassOption,
        verbose_name=_("Class Options"),
        related_name="students",
    )
    # endregion Class data

    class Meta:
        """Class meta for Student."""

        verbose_name = _("Student")
        verbose_name_plural = _("Students")
        ordering = ["last_name", "first_name"]

    def __str__(self):
        """Return the string representation of the student."""
        return self.get_full_name()

    def get_full_name(self):
        """Get the full name of the student."""
        return f"{self.civility.short_name} {self.last_name} {self.first_name}"
