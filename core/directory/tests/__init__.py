"""Tests for the :mod`directory` application."""
import pytest
from directory import models

from .factories import StudentFactory


@pytest.fixture
def student() -> models.Student:
    """Get a single student."""
    _student = StudentFactory()
    return _student


def test_single_student(student: models.Student) -> None:
    """Test a single student."""
    assert student is not None
