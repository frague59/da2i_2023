"""
Factories for :mod:`directory.tests` application.

:creationdate:  12/02/2023 17:56
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: directory.tests.factories
"""
import logging
import random

import factory
from factory.django import DjangoModelFactory

from directory import models

__author__ = "fguerin"
logger = logging.getLogger(__name__)


class StudentFactory(DjangoModelFactory):
    """Factory for students."""

    pk = factory.LazyAttributeSequence(lambda _, n: n)

    civility_id = factory.LazyAttribute(
        lambda _: random.choice([1, 2]),
    )

    last_name = factory.Faker("last_name")

    first_name = factory.Faker("first_name")

    address = factory.Faker("street_address")

    city = factory.Faker("city")

    email = factory.LazyAttribute(
        lambda obj: f"{obj.first_name[0].lower()}{obj.last_name.lower()}@example.com",
    )

    phone_number = factory.Faker("phone_number")

    allow_public_photo = factory.LazyAttribute(
        lambda _: random.choice([True, True, False]),
    )

    class Meta:
        """Metaclass for :class:`directory.tests.factories.StudentFactory`."""

        model = "directory.Student"

    def to_dict(self):
        return {
            "model": "directory.Student",
            "pk": self.pk,
            "fields": {
                "civility_id": self.civility_id,
                "last_name": self.last_name,
                "first_name": self.first_name,
                "email": self.email,
                "phone_number": self.phone_number,
                "address": self.address,
                "city": self.city,
                "allow_public_photo": self.allow_public_photo,
            },
        }
