"""
urls for :mod:`directory` application.

:creationdate:  12/02/2023 19:42
:moduleauthor: François GUÉRIN <fguerin@ville-tourcoing.fr>
:modulename: directory.urls
"""
from django.urls import path
from directory import views

__author__ = "fguerin"

app_name = "directory"

urlpatterns = [
    path("", views.StudentListView.as_view(), name="student_list"),
    path("add/", views.StudentCreateView.as_view(), name="student_add"),
    path("<int:pk>", views.StudentDetailView.as_view(), name="student_detail"),
    path("<int:pk>/update/", views.StudentUpdateView.as_view(), name="student_update"),
]
