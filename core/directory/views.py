"""Views for the :mod:`directory` application."""
import logging

from django.utils.translation import gettext as _
from django.views.generic import ListView, CreateView, UpdateView, DetailView

from directory import models, forms

logger = logging.getLogger(__name__)


class StudentDetailView(DetailView):
    """Detail of a :class:`` instance."""

    model = models.Student

    def get_context_data_data(self, **kwargs):
        """Get context_data for template;"""
        context_data = super().get_context_data_data(**kwargs)
        context_data["page_title"] = _("Students")
        logger.debug(
            "%s::get_context_data_data() context_data = %s",
            self.__class__.__name__,
            context_data,
        )
        return context_data


class StudentListView(ListView):
    """List of a :class:`` instances."""

    model = models.Student

    def get_context_data_data(self, **kwargs):
        """Get context_data for template;"""
        context_data = super().get_context_data_data(**kwargs)
        context_data["page_title"] = "Students"
        logger.debug(
            "%s::get_context_data_data() context_data = %s",
            self.__class__.__name__,
            context_data,
        )
        return context_data


class StudentCreateView(CreateView):
    """View to create a :class:`directory.models.Student` instance."""

    model = models.Student
    form_class = forms.StudentCreateForm

    def get_context_data_data(self, **kwargs):
        """Get context_data for template;"""
        context_data = super().get_context_data_data(**kwargs)
        context_data["page_title"] = _("Create a new student")
        logger.debug(
            "%s::get_context_data_data() context_data = %s",
            self.__class__.__name__,
            context_data,
        )
        return context_data


class StudentUpdateView(UpdateView):
    """View to update a :class:`directory.models.Student` instance."""

    model = models.Student
    form_class = forms.StudentUpdateForm

    def get_context_data_data(self, **kwargs):
        """Get context_data for template;"""
        context_data = super().get_context_data_data(**kwargs)
        context_data["page_title"] = _("Create a student: {student}").format(student=self.object)
        logger.debug(
            "%s::get_context_data_data() context_data = %s",
            self.__class__.__name__,
            context_data,
        )
        return context_data
